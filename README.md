# Workshop Domobox

Home Automation Raspberry Pi stack with Docker

- Raspbian Strech (ARM OS)
- Domoticz (Home Automation)
- Portainer (Docker Monitoring)

## HARDWARE REQUIREMENTS

### Raspberry PI
At least Raspberry PI 2.  
Ex : [Amazon](https://www.amazon.fr/Raspberry-Pi-Official-Desktop-Starter/dp/B01CI58722/ref=sr_1_1_sspa?s=computers&ie=UTF8&qid=1549447637&sr=1-1-spons&keywords=raspberry+pi+3+b+kit&psc=1)  

Warning : The Raspberry PI 2 don't have any Wifi chipset. You need add a wifi dongle.  
Advice : the TP LINK Wifi USB dongle are compatible.  
Ex : [Amazon](https://www.amazon.fr/gp/product/B008IFXQFU/ref=ppx_yo_dt_b_asin_title_o09__o00_s00?ie=UTF8&psc=1)  

### Wifi Module
For the workshop you need this product : TP LINK – HS110 FR  
Ex : [Amazon](https://www.amazon.fr/TP-Link-connect%C3%A9e-consommation-fonctionne-Assistant/dp/B01I59FP3E/ref=sr_1_1?s=computers&ie=UTF8&qid=1549447525&sr=1-1&keywords=TP%2BLINK%2B%E2%80%93%2BHS110&th=1)  
Warning : The Raspberry PI 2 don't have any Wifi chipset. You need add a wifi dongle.  
  
The old model : TP LINK – HS100 FR is compatible with the workshop.  
This old device does not have the measure of consumption  

### Laptop
For the workshop you need a laptop or desktop (Windows, Linux or Mac OS) for SSH communication with the Raspberry PI.  

## SOFTWARE REQUIREMENTS

### Raspbian

#### Download
```sh
https://www.raspberrypi.org/downloads/raspbian/
```
Download the last lite release : Raspbian Stretch Lite

For flashing the SD Card you can use [Etcher](https://www.balena.io/etcher/)
Select image, Select drive and Flash
Put the Card in the Raspberry SD Card slot.

#### Installation
- SSH  

As of the November 2016 release, Raspbian has the SSH server disabled by default. 

For activate the SSH and communicate with the new Raspbian install 

Solution 1  
With screen HDMI / Screen / Keyboard :
You will have to enable it manually. This is done using raspi-config:
Connect with the default id : pi / raspberry
Enter sudo raspi-config in the terminal, first select , then navigate to ssh, press Enter and select Enable or disable ssh server.
```sh
$ sudo raspi-config
```
- Select Interfacing options + Enter
- Select Enable or disable ssh server

Solution 2  
Add a ssh empty file on the root install : 
```sh
ssh
```

- Find your Raspberry on your network  

Solution 1  
Go on your router interface and find the new DHCP assignation

Solution2  
For find the IP address of your Raspberry, you can scan your network with a scanner tool like : [nmap](https://nmap.org/download.html)
Run this command and check the ip list of your devices and find the raspberry ip :
```sh
$ nmap -sP 192.168.x.1/24

Nmap scan report for 192.168.x.x
Host is up (0.0020s latency).
MAC Address: xx:xx:xx:xx:xx:xx (Raspberry Pi Foundation)
```

#### Configuration
Configure all the options you want with the command:
```sh
$ sudo raspi-config
```
- Change the user "pi" password
- Setup wifi configuration (or do it manually with options cf.§Wifi)
- Localisation options : Locale, Timezone, ...

- Serial  

If you use a Smart meter or other devices witch use Serial port communication, you must activate Serial port on the Raspberry
Enter sudo raspi-config in the terminal, first select , then navigate to serial, press Enter and select Enable or disable serial.
```sh
sudo raspi-config
```
- Select Interfacing options + Enter
- Select Enable or disable serial

- Wifi  

Warning: The Wifi interface have another MAC address, you must fix a different IP too on the router DHCP server.
The documentation for WiFi configuration
```sh
https://www.raspberrypi.org/documentation/configuration/wireless/wireless-cli.md
```
```
wpa_passphrase "testing" "testingPassword"
```
```sh
sudo nano /etc/wpa_supplicant/wpa_supplicant.conf
```
Hidden pwd : scan_ssid
```
network={
    ssid="yourHiddenSSID"
    scan_ssid=1
    psk="Your_wifi_password"
}
```

#### Update (optional)
[text](https://pimylifeup.com/upgrade-raspbian-stretch-to-raspbian-buster/)

```sh
sudo apt-get update
sudo apt-get upgrade
sudo apt-get dist-upgrade
```
```sh
pi@raspberrypi:~ $ lsb_release -a
```
```sh
No LSB modules are available.
Distributor ID: Raspbian
Description:    Raspbian GNU/Linux 9.4 (stretch)
Release:        9.4
Codename:       stretch
```

### Git (optional)
#### Installation
```sh
$ apt-get install git
```
  
### Docker
#### Installation
```sh
curl -sSL get.docker.com | sh
```
Check the version 
```sh
docker -v
```  

### Docker-compose installation
#### python
Javabean stop to maintain the repository
So the alternative to install the compose last version with pip
Remove docker compose
```sh
sudo apt-get remove docker-compose
```
If you have install docker-compose with curl in /usr/local/bin/docker-compose
Remove the folder.
```sh
sudo rm -rf /usr/local/bin/docker-compose
```
Install pip and libffi-dev (ffi.h is needed)
```sh
sudo apt-get install python-pip libffi-dev
```
Re-install backports.ssl-match-hostname as package
```sh
sudo pip uninstall backports.ssl-match-hostname
sudo apt-get install python-backports.ssl-match-hostname
```

Install docker-compose
```sh
sudo pip install docker-compose=1.24.0
```

#### javabean
Check the last version on github repository
```sh
https://github.com/javabean/arm-compose/releases/
```  

```sh
$ sudo curl -L https://github.com/javabean/arm-compose/releases/download/{version}/docker-compose-Linux-armv7l -o /usr/local/bin/docker-compose
ex : $ sudo curl -L https://github.com/javabean/arm-compose/releases/download/1.23.2/docker-compose-Linux-armv7l -o /usr/local/bin/docker-compose
```
  
```sh
$ sudo chmod +x /usr/local/bin/docker-compose
``` 

Check the version 
```sh
docker-compose -v
```

## Workshop Domobox Stack

### Get the docker-compose file by git (or copy)
```sh
https://gitlab.com/rpi-domobox/domoticz.git
```

### Build and run  
```sh
# run all images
docker-compose up -d
```

## Domoticz
https://gitlab.com/rpi-domobox/rpi-domoticz

Warning !!
Since the v4.9700 you need add the "libcurl4-gnutls-dev"
In your Dockerfile :
```sh
...
RUN apt-get install -y libcurl4-gnutls-dev
...
```

If you don't have any controller remove the device line in the docker-compose.yml file
```sh
devices:
   - "/dev/ttyACM0"
```

### Fix USB ports
https://www.domoticz.com/wiki/PersistentUSBDevices

Modify the com rules files
```sh
sudo nano /etc/udev/rules.d/99-usb-serial.rules
```
or
```sh
sudo nano /etc/udev/rules.d/99-com.rules

```
Add
```sh
SUBSYSTEM=="tty", ATTRS{idVendor}=="{ID_VENDOR_ID}", ATTRS{idProduct}=="{ID_MODEL_ID}", ATTRS{serial}=="{ID_SERIAL_SHORT}", SYMLINK+="ttyUSBZIGBEE"
```

For Zigbee USB-Stick 
```sh
$ sudo udevadm info --query=all --name=ttyACM0

P: /devices/platform/soc/3f980000.usb/usb1/1-1/1-1.1/1-1.1.3/1-1.1.3:1.0/tty/ttyACM0
N: ttyACM0
S: serial/by-id/usb-Texas_Instruments_TI_CC2531_USB_CDC___0X00124B0018E1CCD0-if00
S: serial/by-path/platform-3f980000.usb-usb-0:1.1.3:1.0
E: DEVLINKS=/dev/serial/by-path/platform-3f980000.usb-usb-0:1.1.3:1.0 /dev/serial/by-id/usb-Texas_Instruments_TI_CC2531_USB_CDC___0X00124B0018E1CCD0-if00
E: DEVNAME=/dev/ttyACM0
E: DEVPATH=/devices/platform/soc/3f980000.usb/usb1/1-1/1-1.1/1-1.1.3/1-1.1.3:1.0/tty/ttyACM0
E: ID_BUS=usb
E: ID_MODEL=TI_CC2531_USB_CDC
E: ID_MODEL_ENC=TI\x20CC2531\x20USB\x20CDC
E: ID_MODEL_ID=16a8
E: ID_PATH=platform-3f980000.usb-usb-0:1.1.3:1.0
E: ID_PATH_TAG=platform-3f980000_usb-usb-0_1_1_3_1_0
E: ID_REVISION=0009
E: ID_SERIAL=Texas_Instruments_TI_CC2531_USB_CDC___0X00124B0018E1CCD0
E: ID_SERIAL_SHORT=__0X00124B0018E1CCD0
E: ID_TYPE=generic
E: ID_USB_CLASS_FROM_DATABASE=Communications
E: ID_USB_DRIVER=cdc_acm
E: ID_USB_INTERFACES=:020201:0a0000:
E: ID_USB_INTERFACE_NUM=00
E: ID_VENDOR=Texas_Instruments
E: ID_VENDOR_ENC=Texas\x20Instruments
E: ID_VENDOR_FROM_DATABASE=Texas Instruments, Inc.
E: ID_VENDOR_ID=0451
E: MAJOR=166
E: MINOR=0
E: SUBSYSTEM=tty
E: TAGS=:systemd:
E: USEC_INITIALIZED=4879253
```

### Zigbee2MQTT plugin
The Zigbee2MQTT plugin is include in the docker image build.
You can add a new ZigBee Gateway hardware directly in the hardware tab.

## Watchtower
### Configuration


## Mosquitto
### Configuration
The container is running with a default configuration.
You can modify configuration in the dedicated volume: mosquitto_config:
```sh
$ sudo docker volume ls
$ sudo docker volume inspect pi_mosquitto_config
$ sudo su
$ cd /var/lib/docker/volumes/pi_mosquitto_config/_data
$ nano mosquitto.conf
```

## Zigbee
#### Zigbee2MQTT
Warning: You need to have a MQTT broker running with your own configuration
If you don't use docker-compose run the container with the following command
```sh
sudo docker run -it -v zigbee_data:/app/data --device=/dev/ttyUSBZIGBEE koenkk/zigbee2mqtt:arm32v6
```
You need to put your own MQTT broker configuration in the dedicated volume.
The configuration file (configuration.yaml) was created in the zigbee_data volume
```sh
$ sudo docker volume ls
$ sudo docker volume inspect pi_zigbee_data
$ sudo su
$ cd /var/lib/docker/volumes/pi_zigbee_data/_data
$ nano configuration.yaml
```

For securized broker
```sh
# Optional: MQTT server authentication user
user: {user}
# Optional: MQTT server authentication password
password: {password}
# Optional: MQTT client ID
client_id: '{client_id}'
```

For fixed ports
```sh
# Serial settings
serial:
  # Location of CC2531 USB sniffer
  port: /dev/ttyUSBZIGBEE
```

### DEBUG
#### How to get bash or ssh into a running container in background mode?
For getting the ID
```sh
sudo docker ps
```
For SSH into container
```sh
sudo docker exec -i -t {container_id} /bin/bash #by ID
```
or
```sh
sudo docker exec -i -t {container_name} /bin/bash #by Name
```

#### Inspect a restarting container
Add in your docker-compose.yml file
```sh
command: tail -f /dev/null
```
And re run your container, you can now inspect your container